<?php

use Illuminate\Database\Seeder;
use App\Currency;
use App\Http\Traits\XmlToJsonTrait;

class CurrenciesTableSeeder extends Seeder
{
    use XmlToJsonTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('currencies')->delete();

        $data = self::dataCBR();

        foreach ($data as $value) {
            Currency::create([
                'item_id'           => $value['item_id'],
                'name'              => $value['name'],
                'english_name'      => $value['english_name'],
                'alphabetic_code'   => $value['alphabetic_code'],
                'digit_code'        => $value['digit_code'],
                'rate'              => $value['rate'],
            ]);
        }
    }
}
