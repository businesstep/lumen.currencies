# lumen.currencies

сервис на Laravel/Lumen 5.*, предоставляющий информацию о валютах в формате JSON

## Установка
- Сколонировать репозиторий:
`git clone https://bitbucket.org/businesstep/lumen.currencies.git`

- Перейти в директорию проекта:
`cd lumen.currencies`

- В файле *.env* указать доступы к БД

- Установить Composer зависимости, выполнив команду:
`composer install`

- Запустить БД миграции, выполнив команду:
`php artisan migrate`

- Запустить команд seed ля заполнения БД данными:
`php artisan db:seed --class=DatabaseSeeder`

## Функционал
 * GET /api/currencies — возвращает список валют со всеми полями
 * GET /api/currencies/{id} — возвращает информацию о валюте для переданного идентификатора

