<?php

namespace App\Http\Traits;


trait XmlToJsonTrait
{
    function dataCBR()
    {
        $xml = self::parse('http://www.cbr.ru/scripts/XML_valFull.asp');
        $json_full = json_decode(json_encode($xml));

        $xml = self::parse('http://www.cbr.ru/scripts/XML_daily.asp');
        $json_curs = json_decode(json_encode($xml));
        $curses = self::valCurs($json_curs);

        return self::toArray($json_full, $curses);
    }

    function parse($url)
    {
        $contents = file_get_contents($url);
        $contents = str_replace(['\n', '\r', '\t'], '', $contents);
        $contents = trim(str_replace('"', "'", $contents));

        return simplexml_load_string($contents);
    }

    function toArray($json, array $curses)
    {
        $data = [];

        foreach ($json->Item as $obj) {
            $obj = json_decode(json_encode($obj),true);

            $data[] = [
                'item_id'           => $obj['@attributes']['ID'],
                'name'              => $obj['Name'],
                'english_name'      => $obj['EngName'],
                'alphabetic_code'   => empty($obj['ISO_Char_Code']) ? '' : $obj['ISO_Char_Code'],
                'digit_code'        => empty($obj['ISO_Num_Code']) ? '' : $obj['ISO_Num_Code'],
                'rate'              => $curses[$obj['@attributes']['ID']] ?? 0
            ];
        }

        return $data;
    }

    function valCurs($json)
    {
        $data = [];

        foreach ($json->Valute as $obj) {
            $obj = json_decode(json_encode($obj),true);

            $data[$obj['@attributes']['ID']] = str_replace(',','.', $obj['Value']);
        }

        return $data;
    }
}