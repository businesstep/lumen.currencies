<?php

namespace App\Http\Controllers;

use App\Currency;

class CurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Currency::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function show($id /*Currency $currency*/)
    {
        $currency = Currency::whereId($id)->firstOrFail();

        return response()->json($currency);
    }
}
