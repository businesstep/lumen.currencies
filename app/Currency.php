<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'item_id', 'name', 'english_name', 'alphabetic_code', 'digit_code', 'rate'
    ];

    public function converting()
    {
        return round(1/$this->rate, 4);
    }

    public function getRouteKeyName()
    {
        return 'id';
    }
}